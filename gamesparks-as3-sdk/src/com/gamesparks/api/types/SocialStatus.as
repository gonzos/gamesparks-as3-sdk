
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

package com.gamesparks.api.types
{

	import com.gamesparks.*;
	
	
	public class SocialStatus extends GSData
	{
	
		public function SocialStatus(data : Object)
		{
			super(data);
		}
	
	
		/// <summary>
		/// When the token is still active.
		/// </summary>
		public function getActive() : Boolean{
			if(data.active != null)
			{
				return data.active;
			}
			return false;
		}
		/// <summary>
		/// When the token expires (if available).
		/// </summary>
		public function getExpires() : Date{
			if(data.expires != null)
			{
				return RFC3339toDate(data.expires);
			}
			return null;
		}
		/// <summary>
		/// The identifier of the external platform.
		/// </summary>
		public function getSystemId() : String{
			if(data.systemId != null)
			{
				return data.systemId;
			}
			return null;
		}
	}

}

