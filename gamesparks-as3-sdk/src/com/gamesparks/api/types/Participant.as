
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

package com.gamesparks.api.types
{

	import com.gamesparks.*;
	
	
	public class Participant extends GSData
	{
	
		public function Participant(data : Object)
		{
			super(data);
		}
	
	
		/// <summary>
		/// The achievements of the Player
		/// </summary>
		public function getAchievements() : Vector.<String>
		{
			var ret : Vector.<String> = new Vector.<String>();

			if(data.achievements != null)
			{
			 	var list : Array = data.achievements;
			 	for(var item : String in list)
			 	{
				 	ret.push(list[item]);
			 	}
			}
			
			return ret;
		}
		/// <summary>
		/// The display name of the Player
		/// </summary>
		public function getDisplayName() : String{
			if(data.displayName != null)
			{
				return data.displayName;
			}
			return null;
		}
		/// <summary>
		/// The external Id's of the Player
		/// </summary>
		public function getExternalIds() : Object{
			if(data.externalIds != null)
			{
				return data.externalIds;
			}
			return null;
		}
		/// <summary>
		/// The id of the Player
		/// </summary>
		public function getId() : String{
			if(data.id != null)
			{
				return data.id;
			}
			return null;
		}
		/// <summary>
		/// The online status of the Player
		/// </summary>
		public function getOnline() : Boolean{
			if(data.online != null)
			{
				return data.online;
			}
			return false;
		}
		/// <summary>
		/// The peerId of this participant within the match
		/// </summary>
		public function getPeerId() : Number{
			if(data.peerId != null)
			{
				return data.peerId;
			}
			return NaN;
		}
		/// <summary>
		/// The script data of the Player
		/// </summary>
		public function getScriptData() : Object{
			if(data.scriptData != null)
			{
				return data.scriptData;
			}
			return null;
		}
		/// <summary>
		/// The virtual goods of the Player
		/// </summary>
		public function getVirtualGoods() : Vector.<String>
		{
			var ret : Vector.<String> = new Vector.<String>();

			if(data.virtualGoods != null)
			{
			 	var list : Array = data.virtualGoods;
			 	for(var item : String in list)
			 	{
				 	ret.push(list[item]);
			 	}
			}
			
			return ret;
		}
	}

}

