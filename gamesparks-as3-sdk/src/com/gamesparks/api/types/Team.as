
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

package com.gamesparks.api.types
{

	import com.gamesparks.*;
	
	
	public class Team extends GSData
	{
	
		public function Team(data : Object)
		{
			super(data);
		}
	
	
		/// <summary>
		/// The team members
		/// </summary>
		public function getMembers() : Vector.<Player>
		{
			var ret : Vector.<Player> = new Vector.<Player>();

			if(data.members != null)
			{
			 	var list : Array = data.members;
			 	for(var item : Object in list)
			 	{
				 	ret.push(new Player(list[item]));
			 	}
			}
			
			return ret;
		}
		/// <summary>
		/// A summary of the owner
		/// </summary>
		public function getOwner() : Player{
			if(data.owner != null)
			{
				return new Player(data.owner);
			}
			return null;
		}
		/// <summary>
		/// The Id of the team
		/// </summary>
		public function getTeamId() : String{
			if(data.teamId != null)
			{
				return data.teamId;
			}
			return null;
		}
		/// <summary>
		/// The team type
		/// </summary>
		public function getTeamType() : String{
			if(data.teamType != null)
			{
				return data.teamType;
			}
			return null;
		}
	}

}

