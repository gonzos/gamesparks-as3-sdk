
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

package com.gamesparks.api.types
{

	import com.gamesparks.*;
	
	
	public class VirtualGood extends GSData
	{
	
		public function VirtualGood(data : Object)
		{
			super(data);
		}
	
	
		/// <summary>
		/// The Windows Phone 8 productId of the item.
		/// </summary>
		public function getWP8StoreProductId() : String{
			if(data.WP8StoreProductId != null)
			{
				return data.WP8StoreProductId;
			}
			return null;
		}
		/// <summary>
		/// The Amazon Store productId of the item.
		/// </summary>
		public function getAmazonStoreProductId() : String{
			if(data.amazonStoreProductId != null)
			{
				return data.amazonStoreProductId;
			}
			return null;
		}
		/// <summary>
		/// The Currency1 cost of the Virtual Good
		/// </summary>
		public function getCurrency1Cost() : Number{
			if(data.currency1Cost != null)
			{
				return data.currency1Cost;
			}
			return NaN;
		}
		/// <summary>
		/// The Currency2 cost of the Virtual Good
		/// </summary>
		public function getCurrency2Cost() : Number{
			if(data.currency2Cost != null)
			{
				return data.currency2Cost;
			}
			return NaN;
		}
		/// <summary>
		/// The Currency3 cost of the Virtual Good
		/// </summary>
		public function getCurrency3Cost() : Number{
			if(data.currency3Cost != null)
			{
				return data.currency3Cost;
			}
			return NaN;
		}
		/// <summary>
		/// The Currency4 cost of the Virtual Good
		/// </summary>
		public function getCurrency4Cost() : Number{
			if(data.currency4Cost != null)
			{
				return data.currency4Cost;
			}
			return NaN;
		}
		/// <summary>
		/// The Currency5 cost of the Virtual Good
		/// </summary>
		public function getCurrency5Cost() : Number{
			if(data.currency5Cost != null)
			{
				return data.currency5Cost;
			}
			return NaN;
		}
		/// <summary>
		/// The Currency6 cost of the Virtual Good
		/// </summary>
		public function getCurrency6Cost() : Number{
			if(data.currency6Cost != null)
			{
				return data.currency6Cost;
			}
			return NaN;
		}
		/// <summary>
		/// The description of the Virtual Good
		/// </summary>
		public function getDescription() : String{
			if(data.description != null)
			{
				return data.description;
			}
			return null;
		}
		/// <summary>
		/// The google play productId of the item.
		/// </summary>
		public function getGooglePlayProductId() : String{
			if(data.googlePlayProductId != null)
			{
				return data.googlePlayProductId;
			}
			return null;
		}
		/// <summary>
		/// The iOS AppStore productId of the item.
		/// </summary>
		public function getIosAppStoreProductId() : String{
			if(data.iosAppStoreProductId != null)
			{
				return data.iosAppStoreProductId;
			}
			return null;
		}
		/// <summary>
		/// The maximum number of the Virtual Good that can be owned at one time
		/// </summary>
		public function getMaxQuantity() : Number{
			if(data.maxQuantity != null)
			{
				return data.maxQuantity;
			}
			return NaN;
		}
		/// <summary>
		/// The name of the Virtual Good
		/// </summary>
		public function getName() : String{
			if(data.name != null)
			{
				return data.name;
			}
			return null;
		}
		/// <summary>
		/// The custom property set configured on the item.
		/// </summary>
		public function getPropertySet() : Object{
			if(data.propertySet != null)
			{
				return data.propertySet;
			}
			return null;
		}
		/// <summary>
		/// The short code of the Virtual Good
		/// </summary>
		public function getShortCode() : String{
			if(data.shortCode != null)
			{
				return data.shortCode;
			}
			return null;
		}
		/// <summary>
		/// The tags of the Virtual Good
		/// </summary>
		public function getTags() : String{
			if(data.tags != null)
			{
				return data.tags;
			}
			return null;
		}
		/// <summary>
		/// The type of the virtual good, "VGOOD" or "CURRENCY"
		/// </summary>
		public function getType() : String{
			if(data.type != null)
			{
				return data.type;
			}
			return null;
		}
		/// <summary>
		/// The Windows 8 productId of the item.
		/// </summary>
		public function getW8StoreProductId() : String{
			if(data.w8StoreProductId != null)
			{
				return data.w8StoreProductId;
			}
			return null;
		}
	}

}

