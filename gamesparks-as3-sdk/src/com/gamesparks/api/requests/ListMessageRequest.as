
package com.gamesparks.api.requests
{

	import com.gamesparks.api.*;
	import com.gamesparks.api.responses.*;
	import com.gamesparks.*;
	
	
	/**
	* Returns the list of the current players messages / notifications.
	* The list only contains un-dismissed messages, to dismiss a message see DismissMessageRequest Read the section on Messages to the the complete list of messages and their meaning.
	*/
	public class ListMessageRequest extends GSRequest
	{
	
		public function setScriptData(scriptData:Object):ListMessageRequest{
			data["scriptData"] = scriptData;
			return this;
		}
		
		function ListMessageRequest(gs:GS)
		{
			super(gs);
			data["@class"] =  ".ListMessageRequest";
		}
		
		/**
		* set the timeout for this request
		*/
		public function setTimeoutSeconds(timeoutSeconds:int=10):ListMessageRequest
		{
			this.timeoutSeconds = timeoutSeconds; 
			return this;
		}
		
		/**
		* Send the request to the server. The callback function will be invoked with the response
		*/
		public override function send (callback : Function) : void{
			super.send( 
				function(message:Object) : void{
					if(callback != null)
					{
						callback(new ListMessageResponse(message));
					}
				}
			);
		}
		



		/**
		* The number of items to return in a page (default=50)
		*/
		public function setEntryCount( entryCount : Number ) : ListMessageRequest
		{
			this.data["entryCount"] = entryCount;
			return this;
		}


		/**
		* An optional filter that limits the message types returned
		*/
		public function setInclude( includeFilter : String ) : ListMessageRequest
		{
			this.data["include"] = includeFilter;
			return this;
		}


		/**
		* The offset (page number) to start from (default=0)
		*/
		public function setOffset( offset : Number ) : ListMessageRequest
		{
			this.data["offset"] = offset;
			return this;
		}

				
	}
	
}

