
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

package com.gamesparks.api.responses
{
	
	import com.gamesparks.api.types.*;
	import com.gamesparks.*;
	
	/**
	* A response to a list team messages request.
	*/
	public class ListTeamChatResponse extends GSResponse
	{
	
		public function ListTeamChatResponse(data : Object)
		{
			super(data);
		}
	
	
	}

}

